package com.example.demo.controller;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.entity.Port;
import com.example.demo.entity.Stock;
import com.example.demo.service.StockService;
import com.example.demo.wrapper.StockWrapper;

@RestController
@EnableAutoConfiguration
@RequestMapping("api/stocks")

public class StockController {

	private static final Logger LOG = LoggerFactory.getLogger(StockController.class);

	@Autowired
	StockService stockService;

	@GetMapping
	public List<Stock> getAllShippers() throws IOException {
		return stockService.getAllStocks();
	}

	@GetMapping("/buy_or_sell/{ticker}")
	@ResponseBody
	public List<StockWrapper> displayStockDetails(@PathVariable("ticker") String ticker) throws IOException {
	   LOG.debug("getShipperById, id=[" + ticker + "]");
		
	   List<StockWrapper> a = new ArrayList<StockWrapper>();
       StockWrapper stockWrapper = stockService.displayStockDetails(ticker);
       
       a.add(stockWrapper);
     
       return a;
	}

	@GetMapping("/buy/{ticker}/{vol}")
	public Stock addStock(Stock stock, @PathVariable("ticker") String ticker, @PathVariable("vol") int vol) throws IOException {
		StockWrapper stockWrapper = stockService.displayStockDetails(ticker);

		stock = new Stock(0, ticker, Timestamp.valueOf(LocalDateTime.now()), vol,
				stockWrapper.getPrice(), "buy");
		
		return stockService.addStock(stock);
	}

	@GetMapping("/sell/{ticker}/{vol}")
	public Stock sellStock(Stock stock, @PathVariable("ticker") String ticker,@PathVariable("vol") int vol) throws IOException {
		StockWrapper stockWrapper = stockService.displayStockDetails(ticker);

		stock = new Stock(0, ticker, Timestamp.valueOf(LocalDateTime.now()), vol,
				stockWrapper.getPrice(), "sell");
		return stockService.sellStock(stock);
	}

	@GetMapping("/view/")
	public List<Stock> getAllStocks(Stock stock) throws IOException {
		return stockService.getAllStocks();
	}

	@GetMapping("/viewport/")
	public List<Port> getAllPort(Stock stock) throws IOException {
		return stockService.getAllPort();
	}
	
	@GetMapping("/update")
	public List<Port> updatePortfolio() {
		return stockService.updatePortfolio();
	}

	@GetMapping("/sum")
	public double getSumStock() {
		return stockService.getSumStock();
	}
	
	@RequestMapping("/testing")
	@ResponseBody
	public String testing() {
		return "hi";
	}
}
