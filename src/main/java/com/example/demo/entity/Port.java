package com.example.demo.entity;

public class Port {
	private String ticker;
	private int volume;
	private double totalPricePaid;
	private double netAssetValue; // currentValue = volume * current value of stocks
    private double returns;
    
    public Port(String ticker,  int volume, double totalPricePaid, double netAssetValue, double returns) {
		super();
		this.ticker = ticker;
		this.volume = volume;
		this.totalPricePaid = totalPricePaid;
		this.netAssetValue = netAssetValue;
		this.returns = returns;
	}
	
	public Port() { }

	public String getTicker() {
		return ticker;
	}

	public int getVolume() {
		return volume;
	}

	public double getTotalPricePaid() {
		return totalPricePaid;
	}

	public double getCurrentValue() {
		return netAssetValue;
	}

	public double getReturns() {
		return returns;
	}
    
}
