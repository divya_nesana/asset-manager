package com.example.demo.wrapper;

import java.time.LocalDateTime;

import yahoofinance.Stock;
import yahoofinance.YahooFinance;

public class StockWrapper {
    private final Stock stock;
    private final LocalDateTime lastAccess;
    private final String symbol;
    private final String name;
    private final double price;
    private final double dayLow;
    private final double dayHigh;
    private final double yearLow;
    private final double yearHigh;
    
    public StockWrapper(Stock stock) {
        this.stock = stock;
        this.lastAccess = LocalDateTime.now();
        
        symbol = stock.getSymbol();
        name = stock.getName();
        price = (stock.getQuote().getPrice()).doubleValue();
        dayLow = (stock.getQuote().getDayLow()).doubleValue();
        dayHigh = (stock.getQuote().getDayHigh()).doubleValue();
        yearLow = (stock.getQuote().getYearLow()).doubleValue();
        yearHigh = (stock.getQuote().getYearHigh()).doubleValue();
    }
    
    public LocalDateTime getLastAccess() {
    	return lastAccess;
    }
    
    public double getPrice() {
    	return price;
    }

	public String getSymbol() {
		return symbol;
	}

	public String getName() {
		return name;
	}

	public double getDayLow() {
		return dayLow;
	}

	public double getDayHigh() {
		return dayHigh;
	}

	public double getYearLow() {
		return yearLow;
	}

	public double getYearHigh() {
		return yearHigh;
	}
}