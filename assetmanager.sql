CREATE DATABASE assetmanager;

DROP DATABASE assetmanager;

USE assetmanager;

CREATE TABLE history
(
      orderID INT NOT NULL AUTO_INCREMENT,
      ticker VARCHAR(10) NOT NULL,
      dateTime TIMESTAMP NOT NULL,
      volume INT NOT NULL,
      valuePerStock DOUBLE NOT NULL,
      buyOrSell VARCHAR(4) NOT NULL,
      PRIMARY KEY(orderID)
);

CREATE TABLE stockdetails
(
      ticker VARCHAR(10) NOT NULL,
      volume INT NOT NULL,
      totalPricePaid DOUBLE NOT NULL,
	  netAssetValue DOUBLE,
      returns DOUBLE,
      PRIMARY KEY(ticker)
);

SELECT * FROM history;
SELECT * FROM stockdetails;

INSERT INTO history VALUES (0,"FB",'2021-08-26 11:45:30',10,350.10);

INSERT INTO stockdetails(ticker,volume,totalPricePaid) VALUES("FB",20,1000);
INSERT INTO stockdetails(ticker,volume,totalPricePaid) VALUES("MSFT",30,50000);
